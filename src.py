
import re

def isVariable(x):
    return x[0] == '_' or x[0].isalpha()

class Add:
    def __init__(self, left, right):
        self.left = left
        self.right = right
    def tree(self):
        return {'op': '+', 'a': self.left.tree()  , 'b':  self.right.tree()}
    def eval(self):
        if type(self.left.eval()) == int and type(self.right.eval()) == int:
            return self.left.eval()+self.right.eval()



class Mul:
    def __init__(self, left, right):
        self.left = left
        self.right = right
    def tree(self):
        return {'op': '*', 'a': self.left.tree()  , 'b':  self.right.tree()  }

class Div:
    def __init__(self, left, right):
        self.left = left
        self.right = right
    def tree(self):
        return {'op': '/', 'a': self.left.tree()  , 'b':  self.right.tree()}


class Sub:
    def __init__(self, left, right):
        self.left = left
        self.right = right
    def tree(self):
        return {'op': '-', 'a': self.left.tree()  , 'b':  self.right.tree()  }

class variable:
    def __init__(self, name, number):
        self.name = name
        self.number = number
    def tree(self):
        return { 'op': 'arg', 'n': self.number }

class num:
    def __init__(self, val):
        self.val = val
    def tree(self):
        return { 'op': 'imm', 'n': int(self.val) }
    def eval(self):
        return int(self.val)

def calc(Q, Pass = 'pass1'):
    arguments = Q[Q.index('[')  + 1: Q.index(']')]
    Q = Q[Q.index(']') + 1:]
    #print(arguments)
    P = []
    def isfloat(x):
        try:
            float(x)
            return True
        except:
            return False
    stack = []
    precedence = {'-' : 1, '+': 1, '*': 2, '/': 2, '%': 2}
    operators = ['+', '/', '*', '-']
    for i in Q:
        if isfloat(i) or isVariable(i):
            P.append(i)
        elif i == '(':
            stack.append(i)
        elif i == ')':
            while stack and stack[-1] != '(':
                P.append(stack.pop())
            if stack[-1] == '(':    stack.pop() 
        elif i in operators:
            if stack == [] or stack[-1] == '(':
                stack.append(i)
            else:
                while stack and stack[-1] != '(' and precedence[i] <= precedence[stack[-1]]:
                    P.append(stack.pop())
                stack.append(i)
    while stack:
        P.append(stack.pop())

    Empty = []
    add = lambda a, b: a + b
    sub = lambda a, b: a - b
    mul = lambda a, b: a * b
    div = lambda a, b: a / b
    ASM = []
    for i in P:
        if isfloat(i):
            if Pass != "pass3":
                Empty.append(i)
            ASM.append("IM {}".format(int(i)))
            ASM.append("PU")
        elif isVariable(i):
            if Pass != "pass3":
                Empty.append(i)
            ASM.append("AR {}".format(arguments.index(i)))
            ASM.append("PU")
        elif i in operators:
            if Pass in ['pass1', 'pass2']:

                a = Empty.pop()
                a = float(a) if isfloat(a) else a
                b = Empty.pop()
                b = float(b) if isfloat(b) else b
                Class = [Add, Div,Mul,Sub, num]
                oper = [add, div, mul, sub]
                
                if type(a) not in Class:
                    if isfloat(a):
                        a = num(a)
                    else:
                        a = variable(a, arguments.index(a))

                if type(b) not in Class:
                    if isfloat(b):
                        b = num(b)
                    else:
                        b = variable(b, arguments.index(b))

                inD = operators.index(i)


                if type(a) == num and type(b) == num and Pass == 'pass2':
                    Empty.append(num(oper[inD](b.val, a.val)))
                else:
                    Empty.append(Class[inD](b, a))
            elif Pass == "pass3":
                operW = ['AD', 'DI', 'MU', 'SU']
                ASM.append('PO')
                ASM.append('SW')
                ASM.append('PO')
                ASM.append(operW[operators.index(i)])
                ASM.append('PU')
    

    
    if Pass == "pass3":
        return ASM
    result = Empty[0]
    return result

class Compiler(object):
    
    def compile(self, program):
        return self.pass3(self.pass2(self.pass1(program)))
        
    def tokenize(self, program):
        token_iter = (m.group(0) for m in re.finditer(r'[-+*/()[\]]|[A-Za-z]+|\d+', program))
        return [int(tok) if tok.isdigit() else tok for tok in token_iter]

    def pass1(self, program):
        tokens = self.tokenize(program)
        self.something = tokens
        return calc(tokens).tree()
        
    def pass2(self, ast):
        return calc(self.something, "pass2").tree()


    def pass3(self, ast):
        return calc(self.something, "pass3")
